package learnexcel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static void main(String[] args) throws IOException {
	   
		XSSFWorkbook wbook = new XSSFWorkbook("./data/input.xlsx"); // open Excel file
		XSSFSheet sheet = wbook.getSheetAt(0); // open sheet
		int rowCnt = sheet.getLastRowNum(); // get the rowcount
		System.out.println("Row Count is " + rowCnt);
		int colCnt = sheet.getRow(0).getLastCellNum(); // get the header and find the last cell no
		System.out.println("Column Count is " +colCnt);
		for (int j = 1; j <= rowCnt ; j++) { // out loop is for row
			XSSFRow row = sheet.getRow(j); // read row
			for (int i = 0; i < colCnt; i++) { // inner loop is for column
				XSSFCell cell = row.getCell(i); // read column
				String CellValue = cell.getStringCellValue(); // get the cell value
				System.out.println(CellValue); // print each cell value
			} 
		}
		wbook.close();
		
	}

}
