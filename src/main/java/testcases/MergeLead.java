package testcases;

//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;

//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class MergeLead extends SeleniumBase{
	@Test
	public void login() throws InterruptedException {
		//Open URL
		startApp("chrome", "http://leaftaps.com/opentaps");
		//Input User Name
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		//Input Password
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		//Click Submit
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//Create Leads
		//Click CRM/SFA Link
		WebElement eleCrmSfa = locateElement("link","CRM/SFA");
		click(eleCrmSfa);
		//Click Leads Link
		WebElement eleLeadsLink = locateElement("link", "Leads");
		click(eleLeadsLink);
		//Click Merge Leads Link
		WebElement eleMergeLeads = locateElement("link", "Merge Leads");
		click(eleMergeLeads);
		
		// Get the Parent Window Id
		//String parentTitle = driver.getTitle();
		//System.out.println(parentTitle);
		//String parentWindow = driver.getWindowHandle();
		//System.out.println(parentWindow);
		String paretWindw = getParentWindow();
		
		//CLick on From Lead image
		WebElement eleFromLeadImg = locateElement("xpath","(//img [@src='/images/fieldlookup.gif'])[1]");
		click(eleFromLeadImg);
		//Switch to New Window
		switchToWindow(2);
		//Enter lead Id
		WebElement eleLeadIdInput = locateElement("name","id");
		clearAndType(eleLeadIdInput,"10053");
		//Click on Find Leads Button
		WebElement eleFindLeadClick = locateElement("xpath","(//button[@class = 'x-btn-text'])[1]");
		click(eleFindLeadClick);
		Thread.sleep(3000);
		//Click on the First Resulting Lead
		WebElement eleFirstLinkClick = locateElement("xpath","(//div[@class = 'x-panel x-grid-panel']//table/following::div/*//td/*/a)[3]");
	    Thread.sleep(3000);
	    clicknew(eleFirstLinkClick);
		System.out.println("CLicked the first lead");
		//switch back to primary parent window
		//driver.switchTo().window(parentWindow);
		switchToWindow(paretWindw);
		
		//CLick on To Lead image
		WebElement eleToLeadImg = locateElement("xpath","(//img [@src='/images/fieldlookup.gif'])[2]");
		click(eleToLeadImg);
		//Switch to New Window
		switchToWindow(2);
		//Enter lead Id
		WebElement eleLeadIdInput1 = locateElement("name","id");
		clearAndType(eleLeadIdInput1,"10056");
		//Click on Find Leads Button
		WebElement eleFindLeadClick1 = locateElement("xpath","(//button[@class = 'x-btn-text'])[1]");
		click(eleFindLeadClick1);
		Thread.sleep(3000);
		//Click on the First Resulting Lead
	    WebElement eleFirstLinkClick1 = locateElement("xpath","(//div[@class = 'x-panel x-grid-panel']//table/following::div/*//td/*/a)[3]");
	    clicknew(eleFirstLinkClick1);
		Thread.sleep(3000);
		
		//switch back to primary window
		//driver.switchTo().window(parentWindow);		
		switchToWindow(paretWindw);
		
		//Click on Merge Lead button
		WebElement eleMergeLeadButton = locateElement("class","buttonDangerous");
		clicknew(eleMergeLeadButton);
		//Accept Alert
		acceptAlert();
		//Display the Merged Lead Name
		WebElement eleCompNameConfirm = locateElement("id","viewLead_companyName_sp");
		System.out.println("The Merged Lead Id is "+ eleCompNameConfirm.getText());;
		//Click Find Leads link
		WebElement eleFirstLeadsLink = locateElement("xpath","//a[@href ='/crmsfa/control/findLeads' or text() ='Find Leads']");
		click(eleFirstLeadsLink);		
		//Enter From Lead Id
		WebElement eleFromLeadText = locateElement("name","id");
		clearAndType(eleFromLeadText,"10053");
		//Click FInd Leads Button
		WebElement eleFindLeadButton = locateElement("xpath","//button[@class ='x-btn-text' and text() ='Find Leads']");
		click(eleFindLeadButton);
		Thread.sleep(3000);	
		
		//Verify the Error Message
		String errorMsg = driver.findElementByClassName("x-paging-info").getText();
				
		if (errorMsg.contains("No records to display")) {
			System.out.println("Test Case verified and Passed");
				
		} else {
					System.out.println("Test Case verified and Failed");
		}

	}
	
}