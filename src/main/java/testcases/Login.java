package testcases;

//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;

//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class Login extends SeleniumBase{
	@Test
	public void login() throws InterruptedException {
		//Open URL
		startApp("chrome", "http://leaftaps.com/opentaps");
		//Input User Name
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		//Input Password
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		//Click Submit
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
	}
}







