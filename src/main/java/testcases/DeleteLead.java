package testcases;

//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;

//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class DeleteLead extends SeleniumBase{
	@Test
	public void login() throws InterruptedException {
		//Open URL
		startApp("chrome", "http://leaftaps.com/opentaps");
		//Input User Name
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		//Input Password
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		//Click Submit
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//Create Leads
		//Click CRM/SFA Link
		WebElement eleCrmSfa = locateElement("link","CRM/SFA");
		click(eleCrmSfa);
		//Click Leads Link
		WebElement eleLeadsLink = locateElement("link", "Leads");
		click(eleLeadsLink);
		//Click Find Leads Link
		WebElement eleFindLeads = locateElement("link", "Find Leads");
		click(eleFindLeads);
		//Enter First Name
		WebElement eleFirstName = locateElement("xpath", "(//div[@class = 'subSectionBlock']//input//preceding::input)[36]");
		clearAndType(eleFirstName,"First name");
		//Click Find Leads Button
		WebElement eleFirstLeadButtn = locateElement("xpath", "//div[@class = 'x-panel-footer x-panel-footer-noborder']/*/*/*/*/*/*");
		click(eleFirstLeadButtn);
		//Click on First Resulting output
		Thread.sleep(3000);
		WebElement eleFirstLead = locateElement("xpath","(//div[@class = 'x-panel x-grid-panel']//table/following::div/*//td/*/a)[1]");
		click(eleFirstLead);
		// Click on Delete Button
		WebElement eleEditButtn = locateElement("xpath","//a[text()='Delete']");
		click(eleEditButtn);
		//Click Find Leads Link
		WebElement eleFindLeadsnew = locateElement("link", "Find Leads");
		click(eleFindLeadsnew);
		//Enter First Name
		WebElement eleFirstNamenew = locateElement("xpath", "(//div[@class = 'subSectionBlock']//input//preceding::input)[36]");
		clearAndType(eleFirstNamenew,"First name");
		//Click Find Leads Button
		WebElement eleFirstLeadButtnnew = locateElement("xpath", "//div[@class = 'x-panel-footer x-panel-footer-noborder']/*/*/*/*/*/*");
		click(eleFirstLeadButtnnew);
		// check the availability of deleted lead
		System.out.println(driver.getPageSource().contains("10095"));
				
							
	}
	
}