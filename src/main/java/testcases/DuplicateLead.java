package testcases;

//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;

//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class DuplicateLead extends SeleniumBase{
	@Test
	public void login() throws InterruptedException {
		//Open URL
		startApp("chrome", "http://leaftaps.com/opentaps");
		//Input User Name
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		//Input Password
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		//Click Submit
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//Create Leads
		//Click CRM/SFA Link
		WebElement eleCrmSfa = locateElement("link","CRM/SFA");
		click(eleCrmSfa);
		//Click Leads link
		WebElement eleLeadsLink = locateElement("link","Leads");
		click(eleLeadsLink);
		//Click Find Leads Link
		WebElement eleFindLeads = locateElement("link","Find Leads");
		click(eleFindLeads);
		//Click on EMail tab
		WebElement eleEmailLink = locateElement("xpath", "//span[text() = 'Email']");
		click(eleEmailLink);		
		//Enter Email Address
		WebElement eleEmailAdd = locateElement("name", "emailAddress");
		clearAndType(eleEmailAdd, "Milan@cts.com");
		//Click Find Leads Button
		WebElement eleFindLeadButtn = locateElement("xpath","(//button[@class = 'x-btn-text'])[7]");
		click(eleFindLeadButtn);
		Thread.sleep(4000);		
		// Capture Name of First resulting lead
		WebElement eleResultText = locateElement("xpath","(//div[@class = 'x-panel x-grid-panel']//table/following::div/*//td/*/a)[3]");
		String ResultName = getElementText(eleResultText);
		System.out.println("Name of First Resulting Lead is " + ResultName);       		
		// Click on First Resulting lead
		WebElement eleClickFirstResult = locateElement("xpath","(//div[@class = 'x-panel x-grid-panel']//table/following::div/*//td/*/a)[1]");
		click(eleClickFirstResult);
		// Click Duplicate lead Button
		WebElement eleDupLeadButtn = locateElement("xpath","((//div[@class = 'frameSectionExtra'])[2]/a)[1]");
		click(eleDupLeadButtn);
		//Verify Page title
		String titleChk = driver.getTitle();
		 if (titleChk.contains("Duplicate Lead")) {
				System.out.println("Title verified and Passed");
		 } else {
				System.out.println("Title verified and Failed");
		 }
		//Click on create Lead
		 WebElement eleCreateLead = locateElement("xpath","(//input[@name = 'submitButton'])[1]");
         click(eleCreateLead);
		// Get the Duplicated Lead Name
         WebElement eleDupLeadNmeChk = locateElement("id", "viewLead_firstName_sp");
         String dupLeadName = getElementText(eleDupLeadNmeChk);
		 System.out.println("Name of duplicate Lead is "+ dupLeadName);
			
			if (ResultName.equalsIgnoreCase(dupLeadName)) {
				System.out.println("Name Verified and Test Case Passed");
			} else {
				System.out.println("Name Verified and Test case Failed");
			}
				
	}
	
}







