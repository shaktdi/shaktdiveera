package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
//import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

import utils.Reports;

public class SeleniumBase extends Reports implements Browser, Element{

	public RemoteWebDriver driver;
	WebDriverWait wait;
	int i = 1;
	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element "+ele+ " clicked", "Pass");
			//System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element "+ele+" could not be clicked", "Fail");
			//System.err.println("The Element "+ele+" could not be clicked");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}
	//Click event method without snapshot to overcome no window found issue
	public void clicknew(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element "+ele+" clicked" , "Pass");
			//System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element "+ele+" could not be clicked", "Fail");
			//System.err.println("The Element "+ele+" could not be clicked");
			throw new RuntimeException();
		}
	}


	@Override
	public void append(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("The given Data ("+data+") is successfully entered in ("+ele+") text field", "pass");
		}catch (ElementNotInteractableException e) {
			reportStep("The Element ("+ele+") is Not interactable/Editable", "fail");			 
			throw new RuntimeException();
		}catch(IllegalArgumentException e){
			reportStep("The given value ("+data+") is Not vaild one/Empty", "fail");
			
		}finally {
		    takeSnap();
		}

	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			reportStep("The Given Element ("+ele+") value get cleared","pass");						
		}catch(InvalidElementStateException e) {			
			reportStep("The Given Element ("+ele+") is not editable","fail");			
		}finally {
			System.out.println("Snapshot Taken after Clear Text Field");
			takeSnap();
		}


	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data :"+data+" entered Successfully", "Pass");
			//System.out.println("The Data :"+data+" entered Successfully");
		} catch (ElementNotInteractableException e) {
			reportStep("The Element "+ele+" is not Interactable", "Fail");
			//System.err.println("The Element "+ele+" is not Interactable");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}

	@Override
	public String getElementText(WebElement ele) {
		String text = "";
		try {
			//String text = ele.getText();
			text = ele.getText();
			//return text;
			reportStep("The Element Text is retrieved", "Pass");
		} catch (Exception e) {
			reportStep("The Element "+ele+" is not Interactable","Fail");
			//System.err.println("The Element "+ele+" is not Interactable");
		} finally {
			takeSnap();
		} return text;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		try {
			String cssValue = ele.getCssValue("background-color");
			reportStep("The Element color is retrieved", "Pass");
			return cssValue;
		} catch (Exception e) {
			reportStep("The Element color cannot be retrieved", "Fail");
		}
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		try {
			String TypedText = ele.getAttribute("value");
			reportStep("The given Element ("+ele+") typed text value ("+TypedText+") is fetched successfully","pass");
			return TypedText;
		} catch (NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return null;
		}
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select select = new Select(ele);
			select.selectByVisibleText(value);
			reportStep("The Selection with visible text ("+value+") is Successfully made","pass");
			}catch(NoSuchElementException e) {
				reportStep("The given Element ("+value+") is not available in the list","fail");
				throw new RuntimeException();
			}catch(StaleElementReferenceException e) {
				reportStep("The List ("+ele+") is not yet loaded","fail");
			}finally {
				takeSnap();
			}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select select = new Select(ele);
			select.selectByIndex(index);		
			reportStep("The Selection with index ("+index+") is Successfully made","pass");
			}catch(StaleElementReferenceException e) {
				reportStep("The List ("+ele+") is not yet loaded","fail");
			}catch(NoSuchElementException e) {
				reportStep("The provided index ("+index+") value is not exist in the list","fail");
				throw new RuntimeException();
			}finally {
				takeSnap();
			}

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
	    try {
			Select srcDrp = new Select(ele);
			srcDrp.selectByValue(value);
			reportStep("The Value in dropdwon selected by Value", "Pass");
		} catch (NoSuchElementException e) {
			reportStep("The Element is not present" , "Fail");
			//System.err.println("The Element is Not Present");
		} finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		String Actualtext = ele.getText();
		try {
		if(Actualtext.equals(expectedText)) {
			reportStep("The Actual Text ("+Actualtext+")  is equal with Expected Text ("+expectedText+")","pass");
			return true;
			}
		else {
			reportStep("The Actual Text ("+Actualtext+") is not equal with Expected Text ("+expectedText+")","pass");
			return false;
		}
		}
		catch(NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}

	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		String Actualtext = ele.getText();
		try {
		if(Actualtext.contains(expectedText)) {
			reportStep("The Actual Text ("+Actualtext+") is cotains Expected Text ("+expectedText+")","pass");
			return true;
			}
		else {
			reportStep("The Actual Text ("+Actualtext+") is not having Expected Text ("+expectedText+")","fail");
			return false;
		}
		}
		catch(NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		String Attributevalue = ele.getAttribute(attribute);
		try {
		if(Attributevalue.equals(value)) {
			reportStep("The Actual Attribute value"+Attributevalue+" is equls with Expected value "+value,"pass");
			return true;}
		else {
			reportStep("The Actual Attribute value"+Attributevalue+" is equls with Expected value "+value,"fail");	
			return false;
		}
		}catch(NoSuchElementException e) {
			reportStep("The given Attribute "+attribute+" is not available for the element "+ele,"fail");
			return false;
		}	

	}

	@Override
	public boolean verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String Attributevalue = ele.getAttribute(attribute);
		try {
		if(Attributevalue.contains(value)) {
			reportStep("The Actual Attribute value"+Attributevalue+" contains Expected value "+value,"pass");
			return true;
			}
		else {
			reportStep("The Actual Attribute value"+Attributevalue+" is not having Expected value "+value,"fail");	
			return false;
		}
		}
		catch(NoSuchElementException e) {
			reportStep("The given Attribute "+attribute+" is not available for the element "+ele,"fail");
			return false;
		}

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		boolean displayed = ele.isDisplayed();
		try {
			if(displayed==true) {
			reportStep("The given Element "+ele+" is displayed","pass");
			return true;
			}else {
				reportStep("The given Element "+ele+" is not displayed","fail");
				return false;
			} 
		} catch (NoSuchElementException e) {
			reportStep("The given Element "+ele+" is not available","fail");
			return false;
		} finally {
			takeSnap();
		}
		
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		boolean displayed = ele.isDisplayed();
		try {
			if(displayed==false) {
			reportStep("The given Element ("+ele+") is not displayed","pass");
			return true;
			}else {
				reportStep("The given Element ("+ele+") is displayed","fail");
				return false;
			} 
		} catch (NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		boolean enabled = ele.isEnabled();
		try {
			if(enabled==true) {
			reportStep("The given Element ("+ele+") is enabled","pass");
			return true;
			}else 
			{
				reportStep("The given Element ("+ele+") is not enabled","fail");
				return false;
			} 
		} 
		catch (NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
			boolean selected = ele.isSelected();
			if(selected==true) {
			reportStep("The given Element ("+ele+") is displayed","pass");
			return true;
			} else 
			{
				reportStep("The given Element ("+ele+") is not displayed","fail");
				return false;
			} 
		} catch (NoSuchElementException e) 
		{
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		} finally {
			takeSnap();
		}
	}

	@Override
	public void startApp(String url) {
		try{
			System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("Application URL launched Successfully", "Pass");
		}catch (Exception e) {			
			reportStep("The Browser Could not be Launched. Hence Failed","fail");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				//ChromeOptions op = new ChromeOptions();
				//op.addArguments("--disable-notifications");
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				driver = new ChromeDriver();
				//driver = new ChromeDriver(op);
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						"./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						"./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("Application URL launched Successfully", "Pass");
		} catch (Exception e) {
			reportStep("The Browser Could not be Launched. Hence Failed", "Fail");
			//System.err.println("The Browser Could not be Launched. Hence Failed");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:"+locatorType+" Not Found with value: "+value);
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		try {
			WebElement ElementbyId = driver.findElementById(value);
			reportStep("The Element is Found with Id value: ("+value+")","pass");
			return ElementbyId;
		} catch (NoSuchElementException e) {
			reportStep("The Element is Not Found with Id value: ("+value+")","fail");
			return null;
		}
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch(type.toLowerCase()) {
			
			case "id": return (List<WebElement>) driver.findElementsById(value);
			case "name": return (List<WebElement>) driver.findElementsByName(value);
			case "class": return (List<WebElement>) driver.findElementsByClassName(value);			
			case "xpath": return (List<WebElement>)  driver.findElementsByXPath(value);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator ("+type+") Not Found with value ("+value+")","fail");
			
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
			reportStep("Switched to Alert successfully","pass");
		} catch (NoAlertPresentException e) {
			reportStep("The Alert Cannot be switched to","fail");
			//System.err.println("The Alert Cannot be switched to");
		} 
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			reportStep("Alert successfully Accepted","pass");
		} catch (NoAlertPresentException e) {
			reportStep("Alert not found","fail");
			//System.err.println("The required Alert is Not Present");
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Alert successfully dismissed","pass");
		} catch (NoAlertPresentException e) {
			//System.err.println("The required Alert is Not Present");
			reportStep("Alert not found","fail");
		}
	}

	@Override
	public String getAlertText() {
		String alertText = "";
		try {
			alertText = driver.switchTo().alert().getText();
			reportStep("Alert text retrieved","pass");
		} catch (NoAlertPresentException e) {
			reportStep("Alert not found","fail");
            //System.err.println("The required Alert is Not Present");
		}
		return alertText;
	}

	@Override
	public void typeAlert(String data) {
		try {
			driver.switchTo().alert().sendKeys(data);
			reportStep("Entered text in Alert","pass");
		} catch (NoAlertPresentException e) {
			reportStep("Alert not found","fail");
		//System.err.println("The required Alert is not present");
		}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allTab = driver.getWindowHandles();
			List<String> allTabList = new ArrayList<>();
			allTabList.addAll(allTab);
			System.out.println(driver.getTitle());
			System.out.println(allTabList);
			driver.switchTo().window(allTabList.get(index));
			System.out.println(driver.getTitle());
		} catch (NoSuchWindowException e) {
			reportStep("The Window does Not Exist","fail");
		}
	}
	
	@Override
	public void switchToWindow(String title) {
		try {
		driver.switchTo().window(title);
		reportStep("Successfully Switch to Window with the Given Title ("+title+")","pass");
		}
	 catch (NoSuchWindowException e) {			
		reportStep("The window is not displayed with the Given Title ("+title+")","fail");
	 }
	}
	
	//To get the Parent Window
	public String getParentWindow() {
		String parentWindow = "";
		try {
			parentWindow = driver.getWindowHandle();
			System.out.println(parentWindow);
			reportStep("Able to Navigate to Parent window", "Pass");
			//return parentWindow;
		} catch (Exception e) {
			reportStep("The Window with\" +parentWindow+ \" not found", "fail");
			//System.err.println("The Window with" +parentWindow+ " not found");
		}
		return parentWindow;
	}

	@Override
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			reportStep("Successfully Switch to Frame with index ("+index+")","pass");
		}catch(NoSuchFrameException e) 
		{
			reportStep("The Frame is not displayed with given index value ("+index+")","fail");
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("Successfully Switch to Default Frame","pass");
		} catch (NoSuchFrameException e) {
			reportStep("Default Frame not found","fail");
			//System.err.println("The Frame does not exist");
		} catch (StaleElementReferenceException e) {
			reportStep("Default Frame not found","fail");
			//System.err.println("Such a Frame is not available in the DOM");
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			reportStep("Successfully Switch to Default Frame","pass");
		} catch (NoSuchFrameException e) {
			reportStep("Not able to switch to Default Frame","fail");
			//System.err.println("The Frame" +idOrName+ "is not available");
		}
	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			reportStep("Successfully Switch to Default Frame","pass");
		} catch (NoSuchFrameException e) {
			reportStep("Not able to switch to Default Frame","fail");
		}
	}

	@Override
	public boolean verifyUrl(String url) {
		try {
			String pageUrl = driver.getCurrentUrl();
			if (pageUrl.equalsIgnoreCase(url)) {
			reportStep("The CurrentUrl ("+pageUrl+") is equals with given Url ("+url+")","pass");
			return true;
			} else 
			{
			reportStep("The CurrentUrl ("+pageUrl+") is not equals with given Url ("+url+")","pass");
			return false;
			}
		} catch (Exception e) {
			reportStep("Unable to Fetch Url","fail");
			return false;
		}
	}

	@Override
	public boolean verifyTitle(String title) {
		try {
			String pageTitle = driver.getTitle();
			if (pageTitle.equalsIgnoreCase(title)) {
			reportStep("The CurrentUrl ("+pageTitle+") is equals with given Url ("+title+")","pass");
			return true;
			} else 
			{
		    reportStep("The CurrentUrl ("+pageTitle+") is not equals with given Url ("+title+")","false");
			return false;
			}
		} catch (Exception e) {
			reportStep("Unable to Fetch current Title to verify with Given Title","fail");
			return false;
		} finally {
			takeSnap();
		}
	}
		
	@Override
	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./Snaps/Snap"+i+".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			System.err.println("Screenshot cannot be taken");
		} i++;
	}

	@Override
	public void close() {
		try {
			driver.close();
			reportStep("The Browser has been closed Successfully","pass");			
		} catch (Exception e) {
			reportStep("Unable to Close the Browser","fail");
		}

	}

	@Override
	public void quit() {
		try {
			driver.quit();
			reportStep("The Browser has been quited Successfully","pass");			
		} catch (Exception e) {
			reportStep("Unable to quit from the Browser","fail");
		}

	}

}
