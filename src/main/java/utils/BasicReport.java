package utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {
	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	
	@BeforeSuite
	public void startReport() {
		//creating a HTML report template
		reporter = new ExtentHtmlReporter("./reports/result.html");
		//Append the results each and every time. don't replace
		reporter.setAppendExisting(true);
		//creating report based on test log
		extent = new ExtentReports();
		//Attach required report with template
		extent.attachReporter(reporter);
	}
	@Test
	public void report() {
		//Defining the test cases
		ExtentTest testcase = extent.createTest("Create Lead", "Creating a Lead");
		//Define the author and category of test case
		testcase.assignAuthor("Shaktdi");
		testcase.assignCategory("Sanity");
		//To print test results in the report
		testcase.pass("User Name entered successfully");
		testcase.fail("Passwprd Not entered");	
	}
	@AfterSuite
	public void stopReport() {
		//save the test results to the file. So far its in temp memory
		extent.flush();
		
	}

}
