package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataLibrary {

	public static Object[][] readExcelData(String excelfileName) throws IOException {
	   
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+excelfileName+".xlsx"); // open Excel file
		XSSFSheet sheet = wbook.getSheetAt(0); // open sheet
		int rowCnt = sheet.getLastRowNum(); // get the rowcount
		System.out.println("Row Count is " + rowCnt);
		int colCnt = sheet.getRow(0).getLastCellNum(); // get the header and find the last cell no
		System.out.println("Column Count is " +colCnt);
		Object [] [] data = new Object [rowCnt][colCnt]; // introduce an empty two dimensional array
		for (int j = 1; j <= rowCnt ; j++) { // out loop is for row
			XSSFRow row = sheet.getRow(j); // read row
			for (int i = 0; i < colCnt; i++) { // inner loop is for column
				XSSFCell cell = row.getCell(i); // read column
				String CellValue = cell.getStringCellValue(); // get the cell value
				data [j-1] [i] = CellValue; // i-1 because i starts from 1 and array starts from 0
				//System.out.println(CellValue); // print each cell value
				System.out.println(data);
			} 
		}
		wbook.close();
		return data;
	}

}
