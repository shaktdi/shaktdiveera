package utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	ExtentTest testcase;
	public String testcaseName , testcaseDesc , author , category;
	public static String excelfileName;
	
	@BeforeSuite (groups = {"all"})
	public void startReport() {
		//creating a HTML report template
		reporter = new ExtentHtmlReporter("./reports/result.html");
		//Append the results each and every time. don't replace
		reporter.setAppendExisting(true);
		//creating report based on test case
		extent = new ExtentReports();
		//Attach required report with template
		extent.attachReporter(reporter);
	}
    @BeforeMethod (groups = {"all"})
	public void testcase() {
		testcase = extent.createTest(testcaseName, testcaseDesc);
		testcase.assignAuthor(author);
		testcase.assignCategory(category);
	}

	
	public void reportStep(String desc , String status) {
		if (status.equalsIgnoreCase("pass")) {
			testcase.pass(desc);
		} else if (status.equalsIgnoreCase("fail")) {
			testcase.fail(desc);	
	}

   }
	
	@AfterSuite
	public void stopReport() {
		//save the test results to the file. So far its in temp memory
		extent.flush();
	}
}
