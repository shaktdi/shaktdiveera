package week6.day11.noon;

//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;

//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class CreateLead extends Annotations{
	
	@BeforeTest (groups = {"all"})
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testcaseDesc = "Create a New Lead in Leaf Taps";
		author = "Shaktdi";
		category = "Sanity";
	}
	@Test (groups = "smoke")
	public void createLead() throws InterruptedException {
		//Click CRM/SFA Link
		WebElement eleCrmSfa = locateElement("link","CRM/SFA");
		click(eleCrmSfa);
		//Click Create Lead
		WebElement eleCreateLead = locateElement("link","Create Lead");
		click(eleCreateLead);
		//Input Company Name
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCompName,"Test Leaf");
		//Input First Name
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(eleFirstName,"First Name");
		//Input Last Name
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLastName,"Last Name");
		//Select from Dropdown
		WebElement elesrcDrpDwn = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingValue(elesrcDrpDwn,"LEAD_EMPLOYEE");
		//Submit the changes
		WebElement elesubmit = locateElement("name","submitButton");
		click(elesubmit);
	}
	
}







