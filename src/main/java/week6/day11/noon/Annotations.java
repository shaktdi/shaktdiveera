package week6.day11.noon;

import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase{
  
  public void f() {
  }
  @BeforeMethod (groups = {"all"})
  public void beforeMethod() {
	//Open URL
	startApp("chrome", "http://leaftaps.com/opentaps");
	//Input User Name
	WebElement eleUserName = locateElement("id", "username");
	clearAndType(eleUserName, "DemoSalesManager");
	//Input Password
    WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, "crmsfa");
	//Click Submit
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);

  }

  @AfterMethod
  public void afterMethod() {
	close();
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
