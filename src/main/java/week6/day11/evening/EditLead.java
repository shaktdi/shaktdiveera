package week6.day11.evening;

//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;

//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class EditLead extends Annotations{
	@BeforeClass (groups = {"all"})
	public void setData() {
		testcaseName = "TC002_Edit Lead";
		testcaseDesc = "Edit an existing Lead in Leaf Taps";
		author = "Shaktdi";
		category = "Sanity";
		excelfileName = "editLead";
	}
	@Test (groups = "sanity" , dependsOnMethods = {"week6.day11.noon.CreateLead.createLead"})
	
	public void editLead() throws InterruptedException {
		//Click CRM/SFA Link
		WebElement eleCrmSfa = locateElement("link","CRM/SFA");
		click(eleCrmSfa);
		//Click Leads Link
		WebElement eleLeadsLink = locateElement("link", "Leads");
		click(eleLeadsLink);
		//Click Find Leads Link
		WebElement eleMergeLeads = locateElement("link", "Find Leads");
		click(eleMergeLeads);
		//Enter First Name
		WebElement eleFirstName = locateElement("xpath", "(//div[@class = 'subSectionBlock']//input//preceding::input)[36]");
		clearAndType(eleFirstName,"name");
		//Click Find Leads Button
		WebElement eleFirstLeadButtn = locateElement("xpath", "//div[@class = 'x-panel-footer x-panel-footer-noborder']/*/*/*/*/*/*");
		click(eleFirstLeadButtn);
		//Click on First Resulting output
		Thread.sleep(3000);
		WebElement eleFirstLead = locateElement("xpath","(//div[@class = 'x-panel x-grid-panel']//table/following::div/*//td/*/a)[1]");
		click(eleFirstLead);
		// Click on Edit Button
		WebElement eleEditButtn = locateElement("xpath","//a[text()='Edit']");
		click(eleEditButtn);
		// Edit Company Name
	    WebElement eleCompName = locateElement("id","updateLeadForm_companyName");
	    clearAndType(eleCompName,"Infosys Technologies");
		// Click Update Button
		WebElement eleUpdateButtn = locateElement("xpath","(//input[@class = 'smallSubmit'])[1]");
		click(eleUpdateButtn);
		// Confirm the Name is changed
		WebElement eleCompNameTxt = locateElement("id","viewLead_companyName_sp");
		String updatedCompName = getElementText(eleCompNameTxt);
		
		if (updatedCompName.contains("Infosys")) {
			System.out.println("Company Name Verified and Passed");
		} else 
		{
			System.out.println("Company Name Verified and Failed");
		}		
	}
	
}







