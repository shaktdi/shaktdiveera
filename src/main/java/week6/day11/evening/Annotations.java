package week6.day11.evening;

import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;
import com.yalla.selenium.api.base.SeleniumBase;

import utils.DataLibrary;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase{
  
  @Parameters({"url","username","password"})
  @BeforeMethod
  public void beforeMethod(String url , String username , String password) 
  {
	//Open URL
	startApp("chrome", url);
	//Input User Name
	WebElement eleUserName = locateElement("id", "username");
	clearAndType(eleUserName, username);
	//Input Password
    WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, password);
	//Click Submit
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);

  }
  
  @DataProvider(name = "createData")
	public Object[][] fetchData () throws IOException 
	{
	 return DataLibrary.readExcelData(excelfileName);
	}
  

  @AfterMethod
  public void afterMethod() {
	close();
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
