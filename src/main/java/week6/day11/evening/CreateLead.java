package week6.day11.evening;

import java.io.IOException;

//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;

//import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

import utils.DataLibrary;

public class CreateLead extends Annotations{
	
	@BeforeClass
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testcaseDesc = "Create a New Lead in Leaf Taps";
		author = "Shaktdi";
		category = "Sanity";
		excelfileName = "input";
	}
	@Test (dataProvider ="createData")
	public void createLead(String cName, String fName, String lName) throws InterruptedException {
		//Click CRM/SFA Link
		WebElement eleCrmSfa = locateElement("link","CRM/SFA");
		click(eleCrmSfa);
		//Click Create Lead
		WebElement eleCreateLead = locateElement("link","Create Lead");
		click(eleCreateLead);
		//Input Company Name
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCompName,cName);
		//Input First Name
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(eleFirstName,fName);
		//Input Last Name
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLastName,lName);
		//Select from Dropdown
		WebElement elesrcDrpDwn = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingValue(elesrcDrpDwn,"LEAD_EMPLOYEE");
		//Submit the changes
		WebElement elesubmit = locateElement("name","submitButton");
		click(elesubmit);
	}
	
//	@DataProvider(name = "createData")
//	public Object[][] fetchData () throws IOException 
//	{
//	 return DataLibrary.readExcelData(excelfileName);
////	 Object [] [] data = new Object[2][3];
////	 
////	 data [0] [0] = "Test leaf";
////	 data [0] [1] = "Sarath";
////	 data [0] [2] = "M";
////	 
////	 data [1] [0] = "TL";
////	 data [1] [1] = "Koushik";
////	 data [1] [2] = "C";
////	 
////	 return data;
//	}
}




